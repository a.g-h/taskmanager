**Steps to run:**

TaskManager is an API of REST endpoints to manage To-do lists with tasks.

1) run `./run-local.sh` from /devtools to start MySQL instance
2) run BackendApplication from /backend directory
3) execute `../backend/resources/create-schemas.sql` to create schemas
4) start JavaFX UI

**TaskManager App consists from:**
1) Spring Boot backend (Spring Data Jpa, Web, MySQL) with TestContainers + Groovy tests
2) JavaFX UI making request using Retrofit HTTP client
3) Devtools to create MySQL docker instance locally (**You'll need docker-compose installed locally**)

To build backed service run:
`./gradlew clean build`
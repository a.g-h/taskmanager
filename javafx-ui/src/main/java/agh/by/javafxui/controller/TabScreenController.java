package agh.by.javafxui.controller;

import agh.by.javafxui.client.domain.Task;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class TabScreenController implements Initializable {

    public TableView<Task> taskTable;
    public TableColumn<Task, String> titleColumn;
    public TableColumn<Task, String> noteColumn;
    public TableColumn<Task, Boolean> doneColumn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        noteColumn.setCellValueFactory(new PropertyValueFactory<>("note"));
        doneColumn.setCellFactory(CheckBoxTableCell.forTableColumn(doneColumn));
        doneColumn.setCellValueFactory(cellData -> cellData.getValue().checked());
    }
}

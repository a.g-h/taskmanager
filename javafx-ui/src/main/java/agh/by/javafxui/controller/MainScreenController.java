package agh.by.javafxui.controller;

import agh.by.javafxui.client.RetrofitServer;
import agh.by.javafxui.client.api.TaskApi;
import agh.by.javafxui.client.api.TodoListApi;
import agh.by.javafxui.client.domain.Task;
import agh.by.javafxui.client.domain.TodoList;
import agh.by.javafxui.client.domain.UpdateStatus;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Response;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainScreenController implements Initializable {

    public TabPane todoListTab;
    public Menu clearListMenu;
    public MenuItem addListMenu;
    public MenuItem addTaskMenu;
    public MenuItem editTaskMenu;
    public MenuItem editListMenu;
    public MenuItem deleteListMenu;
    public MenuItem deleteTaskMenu;

    private static final Logger LOG = LoggerFactory.getLogger(MainScreenController.class);

    private static TaskApi taskApi = RetrofitServer.getInstance().create(TaskApi.class);
    private static TodoListApi todoListApi = RetrofitServer.getInstance().create(TodoListApi.class);

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            Response<List<TodoList>> response = todoListApi.getAll().execute();
            if (response.code() == 200 && !response.body().isEmpty()) {
                Platform.runLater(
                        () -> response.body().forEach(
                                list -> {
                                    Tab newTab = new Tab(list.getTitle());

                                    todoListTab.getTabs().add(newTab);
                                    newTab.setId(list.getId());

                                    FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("new_tab_screen.fxml"));
                                    try {
                                        newTab.setContent(loader.load());
                                    } catch (IOException e) {
                                        LOG.error(e.getMessage());
                                    }

                                    TableView<Task> table = (TableView) newTab.getContent().lookup("#taskTable");

                                    try {
                                        Response<List<Task>> tasks = taskApi.getAll(list.getId()).execute();
                                        if (tasks.code() == 200 && !tasks.body().isEmpty()) {
                                            for (Task task : tasks.body()) {
                                                task.setChecked(new SimpleBooleanProperty(task.getIsDone()));
                                                task.checked().addListener((obs, oldValue, newValue)
                                                        -> {
                                                    try {
                                                        Response<Task> taskResponse = taskApi.updateStatus(todoListTab.getSelectionModel().getSelectedItem().getId(), task.getId(), new UpdateStatus(newValue)).execute();
                                                        if (taskResponse.code() == 200)
                                                            task.setIsDone(taskResponse.body().getIsDone());
                                                    } catch (IOException e) {
                                                        LOG.error(e.getMessage());
                                                        showErrorAlert();
                                                    }
                                                });
                                            }
                                            table.getItems().addAll(tasks.body());
                                        }
                                    } catch (IOException e) {
                                        LOG.error(e.getMessage());
                                    }
                                }
                        )
                );
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        clearListMenu.setDisable(true);
    }

    public void addNewTask(ActionEvent actionEvent) {
        Dialog<Task> dialog = new Dialog<>();
        dialog.setTitle("New task");
        dialog.setHeaderText("Please specify…");
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        TextField title = new TextField("Title");
        TextField note = new TextField("Note");

        dialogPane.setContent(new VBox(8, title, note));
        Platform.runLater(title::requestFocus);
        dialog.setResultConverter((ButtonType button) -> {
            if (button == ButtonType.OK) {
                return Task.builder()
                        .title(title.getText())
                        .note(note.getText())
                        .build();
            }
            return null;
        });
        Optional<Task> optionalResult = dialog.showAndWait();

        optionalResult.ifPresent(this::addTask);
    }

    public void addTodoList(ActionEvent actionEvent) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Add new to-do list");
        dialog.setHeaderText("New to-do list");
        dialog.setContentText("Title:");

        Optional<String> result = dialog.showAndWait();

        result.ifPresent(this::addNewList);
    }

    public void clearList(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to remove all tasks?", ButtonType.OK,
                ButtonType.CANCEL);

        alert.setTitle("Clear to-do list");
        Optional<ButtonType> result = alert.showAndWait();

        if (result.isPresent() && result.get() == ButtonType.OK) {
            //todo db call
        }
    }

    public void addNewList(String title) {
        TodoList newList = TodoList.builder().title(title).build();

        try {
            Response<TodoList> response = todoListApi.create(newList).execute();
            if (response.code() == 201) {
                Platform.runLater(
                        () -> {
                            Tab newTab = new Tab(response.body().getTitle());

                            todoListTab.getTabs().add(newTab);
                            newTab.setId(response.body().getId());

                            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("new_tab_screen.fxml"));
                            try {
                                newTab.setContent(loader.load());
                            } catch (IOException e) {
                                LOG.error(e.getMessage());
                            }
                        }
                );
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    public void addTask(Task task) {
        Tab selectedItem = todoListTab.getSelectionModel().getSelectedItem();

        TableView<Task> table = (TableView) selectedItem.getContent().lookup("#taskTable");

        try {
            Response<Task> response = taskApi.create(selectedItem.getId(), task).execute();
            if (response.code() == 201) {
                Platform.runLater(
                        () -> {
                            Task created = response.body();
                            created.setChecked(new SimpleBooleanProperty(response.body().getIsDone()));
                            created.checked().addListener((obs, oldValue, newValue)
                                    -> {
                                try {
                                    Response<Task> updateResponse = taskApi.updateStatus(selectedItem.getId(), created.getId(), new UpdateStatus(newValue)).execute();
                                    if (response.code() == 200)
                                        created.setIsDone(updateResponse.body().getIsDone());
                                } catch (IOException e) {
                                    LOG.error(e.getMessage());
                                }
                            });
                            table.getItems().add(created);
                        }
                );
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    public void deleteList(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to remove to-do list?", ButtonType.OK,
                ButtonType.CANCEL);

        alert.setTitle("Delete to-do list");
        Optional<ButtonType> result = alert.showAndWait();

        Tab selectedItem = todoListTab.getSelectionModel().getSelectedItem();

        if (result.isPresent() && result.get() == ButtonType.OK) {
            try {
                Response<Void> response = todoListApi.delete(selectedItem.getId()).execute();
                if (response.code() == 200) {
                    Platform.runLater(
                            () -> todoListTab.getTabs().remove(selectedItem)
                    );
                }
            } catch (IOException e) {
                LOG.error(e.getMessage());
            }
        }
    }

    public void deleteTask(ActionEvent actionEvent) {
        Tab tab = todoListTab.getSelectionModel().getSelectedItem();

        TableView<Task> table = (TableView) tab.getContent().lookup("#taskTable");

        Task task = table.getSelectionModel().getSelectedItem();

        try {
            Response<Void> response = taskApi.delete(tab.getId(), task.getId()).execute();
            if (response.code() == 200) {
                Platform.runLater(() -> table.getItems().remove(task));
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    public void editTask(ActionEvent actionEvent) {
        Tab selectedItem = todoListTab.getSelectionModel().getSelectedItem();

        TableView<Task> table = (TableView) selectedItem.getContent().lookup("#taskTable");
        Task selectedTask = table.getSelectionModel().getSelectedItem();
        final int selectedIndex = table.getSelectionModel().getSelectedIndex();

        Dialog<Task> dialog = new Dialog<>();
        dialog.setTitle("Edit task");
        dialog.setHeaderText("Please specify…");
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        TextField title = new TextField(selectedTask.getTitle());
        TextField note = new TextField(selectedTask.getNote());

        dialogPane.setContent(new VBox(8, title, note));
        Platform.runLater(title::requestFocus);
        dialog.setResultConverter((ButtonType button) -> {
            if (button == ButtonType.OK) {
                selectedTask.setTitle(title.getText());
                selectedTask.setNote(note.getText());
                return selectedTask;
            }
            return null;
        });
        Optional<Task> optionalResult = dialog.showAndWait();

        optionalResult.ifPresent(task -> editTask(task, selectedIndex));
    }

    private void editTask(Task task, int selectedIndex) {
        Tab selectedItem = todoListTab.getSelectionModel().getSelectedItem();

        TableView<Task> table = (TableView) selectedItem.getContent().lookup("#taskTable");

        task.setChecked(new SimpleBooleanProperty(false));
        task.checked().addListener((obs, oldValue, newValue)
                -> {
            try {
                Response<Task> response = taskApi.updateStatus(selectedItem.getId(), task.getId(), new UpdateStatus(newValue)).execute();
                if (response.code() == 200)
                    task.setIsDone(response.body().getIsDone());
            } catch (IOException e) {
                LOG.error(e.getMessage());
            }
        });

        try {
            Response<Task> response = taskApi.update(selectedItem.getId(), task.getId(), task).execute();
            if (response.code() == 200) {
                Platform.runLater(
                        () -> table.getItems().set(selectedIndex, response.body())
                );
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    public void editList(ActionEvent actionEvent) {
        final Tab selectedItem = todoListTab.getSelectionModel().getSelectedItem();

        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Edit to-do list");
        dialog.setContentText("Title:");

        Optional<String> result = dialog.showAndWait();

        result.ifPresent(title -> editList(title, selectedItem));
    }

    public void editList(String title, Tab tab) {
        TodoList newList = TodoList.builder().title(title).build();
        try {
            Response<TodoList> response = todoListApi.update(tab.getId(), newList).execute();
            if (response.code() == 200) {
                Platform.runLater(
                        () -> tab.setText(response.body().getTitle())
                );
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    public void showErrorAlert() {
        Alert alert = new Alert(Alert.AlertType.ERROR);

        alert.setHeaderText("Something went wrong!");

        alert.setContentText("Check if back-end app is running");

        alert.showAndWait();
    }
}

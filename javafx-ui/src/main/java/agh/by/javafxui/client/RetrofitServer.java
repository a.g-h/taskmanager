package agh.by.javafxui.client;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitServer {
    private static RetrofitServer instance = new RetrofitServer();

    private Retrofit retrofit;

    public static Retrofit getInstance()
    {
        return instance.retrofit;
    }

    RetrofitServer()
    {
        //todo
        final String defaultUrl = "http://localhost:8080/";
        this.retrofit = new Retrofit.Builder()
                .baseUrl(defaultUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}

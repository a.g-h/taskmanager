package agh.by.javafxui.client;

public class Constants {
    private Constants() {
    }

    public static final String ID = "id";

    //urls
    public static final String BY_ID = "/{id}";
    public static final String DONE = "/done";
    public static final String TODO_URL = "todo-list";
    public static final String TASK_URL = "todo-list/{listId}/task";
}

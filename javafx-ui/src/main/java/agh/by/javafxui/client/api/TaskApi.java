package agh.by.javafxui.client.api;

import agh.by.javafxui.client.domain.Task;
import agh.by.javafxui.client.domain.UpdateStatus;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

import static agh.by.javafxui.client.Constants.*;

public interface TaskApi {
    @POST(TASK_URL)
    Call<Task> create(@Path("listId") String listId, @Body Task task);

    @GET(TASK_URL + BY_ID)
    Call<Task> getById(@Path("listId") String listId, @Path(ID) String id);

    @GET(TASK_URL)
    Call<List<Task>> getAll(@Path("listId") String listId);

    @PUT(TASK_URL + BY_ID)
    Call<Task> update(@Path("listId") String listId, @Path(ID) String id, @Body Task task);

    @DELETE(TASK_URL + BY_ID)
    Call<Void> delete(@Path("listId") String listId, @Path(ID) String id);

    @DELETE(TASK_URL)
    Call<Void> deleteAll(@Path("listId") String listId);

    @PUT(TASK_URL + BY_ID + DONE)
    Call<Task> updateStatus(@Path("listId") String listId, @Path(ID) String id, @Body UpdateStatus status);
}

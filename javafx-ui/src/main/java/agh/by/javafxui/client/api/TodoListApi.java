package agh.by.javafxui.client.api;

import agh.by.javafxui.client.domain.TodoList;
import agh.by.javafxui.client.domain.UpdateStatus;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

import static agh.by.javafxui.client.Constants.*;

public interface TodoListApi {
    @POST(TODO_URL)
    Call<TodoList> create(@Body TodoList list);

    @GET(TODO_URL + BY_ID)
    Call<TodoList> getById(@Path(ID) String id);

    @GET(TODO_URL)
    Call<List<TodoList>> getAll();

    @PUT(TODO_URL + BY_ID)
    Call<TodoList> update(@Path(ID) String id, @Body TodoList task);

    @DELETE(TODO_URL + BY_ID)
    Call<Void> delete(@Path(ID) String id);

    @PUT(TODO_URL + BY_ID + DONE)
    Call<TodoList> updateStatus(@Path(ID) String id, @Body UpdateStatus status);
}

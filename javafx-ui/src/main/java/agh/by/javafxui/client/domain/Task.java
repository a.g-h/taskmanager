package agh.by.javafxui.client.domain;

import javafx.beans.property.SimpleBooleanProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Task {
    private String id;
    private String title;
    private String note;
    private Boolean isDone;
    private SimpleBooleanProperty checked;

    public final boolean isChecked() {
     return checked.get();
    }

    public SimpleBooleanProperty checked() {
       return checked;
    }
}

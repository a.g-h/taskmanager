package agh.by.backend.common

import org.springframework.http.HttpStatus
import org.testng.annotations.*

import static org.junit.Assert.assertEquals
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.testng.Assert.assertTrue

abstract class BaseCrudTest<D extends TestData, E extends BaseEndpoint> extends BaseTest {

    D testData
    E endpoint

    abstract D createTestDataGenerator()

    abstract void init(D testData)

    @BeforeClass
    void beforeClass() {
        testData = createTestDataGenerator()
    }

    @BeforeMethod
    void beforeMethod() {
        testData.cleanParameters()

        init(testData)

        if (endpoint == null) {
            throw new IllegalStateException("Endpoint should be provided")
        }
    }

    @AfterMethod
    void cleanup() {
        this.testData.cleanParameters()
        this.endpoint = null
    }

    @DataProvider(name = "badRequestDataProvider")
    Object[][] getBadRequestData() {
        return testData.getBadRequestData()
    }

    @Test
    void testCreate() {
        def content = testData.getCreateTestData()
        def response = endpoint.create(content, status().isCreated())

        content.each { entry -> assertTrue(response.getAt(entry.key).equals(entry.value), "Response has no $entry.key with $entry.value") }
    }

    @Test(dataProvider = "badRequestDataProvider")
    void testCreate_BadRequest(HttpStatus expectedStatus, def badContent) {
        def request = testData.getCreateTestData() + badContent
        def errorResponse = endpoint.create(request, status().is(expectedStatus.value()))
    }

    @Test
    void testGet() {
        def content = testData.getCreateTestData()
        def createdEntity = endpoint.create(content)
        def id = createdEntity.id

        def response = endpoint.getById(id, status().isOk())
        assertEquals(createdEntity, response)
    }

    @Test
    void testGet_WrongId() {
        def content = testData.getCreateTestData()
        def createdEntity = endpoint.create(content)
        content.each { entry -> assertTrue(createdEntity.getAt(entry.key).equals(entry.value), "Response has no $entry.key with $entry.value") }

        endpoint.getById(UUID.randomUUID(), status().isNotFound())
    }

    @Test
    void testGetAll() {
        def content1 = testData.getCreateTestData()
        def createdEntity1 = endpoint.create(content1)
        def content2 = testData.getCreateTestData()
        def createdEntity2 = endpoint.create(content2)

        def response = endpoint.getAll(status().isOk())
        assertTrue(response.size >= 2)
        response.containsAll([createdEntity1, createdEntity2])
    }

    @Test
    void testUpdate() {
        def content = testData.getCreateTestData()
        def createdEntity = endpoint.create(content)
        def id = createdEntity.id
        def updateContent = testData.getUpdateTestData()

        def updatedEntity = endpoint.update(id, updateContent, status().isOk())

        assertEquals(updatedEntity.id, id)
        updateContent.each { entry -> assertTrue(updatedEntity.getAt(entry.key).equals(entry.value), "Response has no $entry.key with $entry.value") }

        def response = endpoint.getById(id)
        assertEquals(response, updatedEntity)
    }

    @Test(dataProvider = "badRequestDataProvider")
    void testUpdate_BadRequest(HttpStatus expectedStatus, def badContent) {
        def content = testData.getCreateTestData()
        def createdEntity = endpoint.create(content)
        def id = createdEntity.id

        def request = testData.getUpdateTestData() + badContent

        def errorResponse = endpoint.update(id, request, status().is(expectedStatus.value()))

        //ensure nothing changed
        def response = endpoint.getById(id)
        assertEquals(response, createdEntity)
    }

    @Test
    void testDelete() {
        def content = testData.getCreateTestData()
        def createdEntity = endpoint.create(content)
        def id = createdEntity.id

        endpoint.delete(id, status().isOk())

        endpoint.getById(id, status().isNotFound())
    }

}

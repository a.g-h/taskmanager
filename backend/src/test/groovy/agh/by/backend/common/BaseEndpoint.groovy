package agh.by.backend.common

import groovy.json.JsonSlurper
import org.springframework.http.HttpMethod
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders

import static groovy.json.JsonOutput.toJson
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

abstract class BaseEndpoint {

    private JsonSlurper jsonSlurper = new JsonSlurper()

    String endpointPath
    MockMvc mvc

    BaseEndpoint(BaseTest baseTest, String path)
    {
        this.mvc = baseTest.mvc()
        this.endpointPath = path
    }

    def create(
            def content,
            ResultMatcher expectedStatus = status().isCreated()) {
        MvcResult result = createRequest(
                HttpMethod.POST,
                "/${endpointPath}",
                expectedStatus,
                content
        )

        return parseJson(result)
    }

    def getById(
            def id,
            ResultMatcher expectedStatus = status().isOk()) {
        MvcResult result = createRequest(
                HttpMethod.GET,
                "/${endpointPath}/${id}",
                expectedStatus
        )

        return parseJson(result)
    }

    def getAll(
            ResultMatcher expectedStatus = status().isOk()) {
        MvcResult result = createRequest(
                HttpMethod.GET,
                "/${endpointPath}",
                expectedStatus
        )

        return parseJson(result)
    }

    def update(
            def id,
            def content,
            ResultMatcher expectedStatus = status().isOk()) {
        MvcResult result = createRequest(
                HttpMethod.PUT, "/${endpointPath}/${id}",
                expectedStatus,
                content
        )

        return parseJson(result)
    }

    def delete(
            def id,
            ResultMatcher expectedStatus = status().isOk())
    {
        MvcResult result = createRequest(
                HttpMethod.DELETE,
                "/${endpointPath}/${id}",
                expectedStatus
        )
    }

    MvcResult createRequest(HttpMethod method, String url, ResultMatcher expectedStatus = status().isOk(),
                            def content = [:]) {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.request(method, url)
                .content(toJson(content))

        ResultActions result = mvc.perform(requestBuilder)
                .andDo(print())
                .andExpect(expectedStatus)

        return result.andReturn()
    }

    def parseJson(MvcResult result) {
        return jsonSlurper.parseText(result.getResponse().getContentAsString())
    }
}

package agh.by.backend.common

abstract class TestData {

    void cleanParameters()
    {

    }

    abstract def getCreateTestData()

    abstract def getUpdateTestData()

    Object[][] getBadRequestData()
    {
        return []
    }
}

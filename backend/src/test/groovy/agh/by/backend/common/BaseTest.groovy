package agh.by.backend.common

import agh.by.backend.common.listener.MySQLContainerListener
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlScriptsTestExecutionListener
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import org.testng.annotations.BeforeClass
import org.testng.annotations.Listeners

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

@SpringBootTest
@TestExecutionListeners([SqlScriptsTestExecutionListener.class])
@Listeners([MySQLContainerListener.class])
abstract class BaseTest extends AbstractTestNGSpringContextTests {

    @Autowired
    protected WebApplicationContext context

    @Autowired
    protected ObjectMapper objectMapper

    private MockMvc mvc

    MockMvc mvc() {
        return mvc
    }

    @BeforeClass
    void setup() {
        setupMockMvc()
    }

    private void setupMockMvc() {
        this.mvc = MockMvcBuilders.webAppContextSetup(context)
                .defaultRequest(get("/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding('UTF-8'))
                .build()
    }
}

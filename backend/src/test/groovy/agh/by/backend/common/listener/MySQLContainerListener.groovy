package agh.by.backend.common.listener

import common.container.DatabaseContainerApplication
import org.testng.IExecutionListener

class MySQLContainerListener implements IExecutionListener
{
    private DatabaseContainerApplication app

    @Override
    void onExecutionStart()
    {
        app = DatabaseContainerApplication.start()
        app.startDBContainer()
    }

    @Override
    void onExecutionFinish()
    {
        app.stopDBContainer()
    }
}

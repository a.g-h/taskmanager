package agh.by.backend.task

import agh.by.backend.common.BaseEndpoint
import agh.by.backend.common.BaseTest
import org.springframework.http.HttpMethod
import org.springframework.test.web.servlet.ResultMatcher

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class TaskEndpoint extends BaseEndpoint{

    TaskEndpoint(BaseTest baseTest, def listId) {
        super(baseTest, "todo-list/${listId}/task")
    }

    def updateDoneStatus(
            def id,
            def content,
            ResultMatcher expectedStatus = status().isOk())
    {
        def result = createRequest(
                HttpMethod.PUT, "/${endpointPath}/${id}/done",
                expectedStatus, content
        )

        return parseJson(result)
    }
}

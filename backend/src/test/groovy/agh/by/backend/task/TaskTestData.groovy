package agh.by.backend.task

import agh.by.backend.common.TestData
import org.springframework.http.HttpStatus

class TaskTestData extends TestData {

    def parentList

    @Override
    void cleanParameters() {
        parentList = null
    }

    @Override
    def getCreateTestData() {
        return [
                title: 'Bread',
                note: 'Mag with seeds'
        ]
    }

    @Override
    def getUpdateTestData() {
        return [
                title: 'Milk',
                note: 'Prostokvashino'
        ]
    }

    @Override
    Object[][] getBadRequestData() {
        return [
                [HttpStatus.BAD_REQUEST, [title: null]],
                [HttpStatus.BAD_REQUEST, [title: 'a' * 51]],
                [HttpStatus.BAD_REQUEST, [note: 'a' * 501]]
        ]
    }
}

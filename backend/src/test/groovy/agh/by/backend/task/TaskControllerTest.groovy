package agh.by.backend.task

import agh.by.backend.common.BaseCrudTest
import agh.by.backend.todolist.TodoListEndpoint
import org.springframework.test.context.jdbc.Sql
import org.testng.annotations.Test

import static org.junit.Assert.assertEquals

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:create-schemas.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:cleanup.sql")
class TaskControllerTest extends BaseCrudTest<TaskTestData, TaskEndpoint> {

    @Test
    initContext() {

    }

    @Override
    TaskTestData createTestDataGenerator() {
        return new TaskTestData()
    }

    @Override
    void init(TaskTestData testData) {
        TodoListEndpoint listEndpoint = new TodoListEndpoint(this)
        def parentList = listEndpoint.create([
                "title" : "Shopping"
        ])

        testData.parentList = parentList
        this.endpoint = new TaskEndpoint(this, parentList.id)
    }

    @Test
    void testUpdateDoneStatus()
    {
        def created = endpoint.create(testData.getCreateTestData())
        def updated = endpoint.updateDoneStatus(created.id, ["done" : true])
        def retrieved = endpoint.getById(created.id)
        assertEquals(updated, retrieved)
    }
}
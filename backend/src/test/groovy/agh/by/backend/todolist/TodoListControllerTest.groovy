package agh.by.backend.todolist

import agh.by.backend.common.BaseCrudTest
import org.springframework.test.context.jdbc.Sql
import org.testng.annotations.Test

import static org.junit.Assert.assertEquals

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:create-schemas.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:cleanup.sql")
class TodoListControllerTest extends BaseCrudTest<TodoListTestData, TodoListEndpoint> {

    @Test
    initContext() {

    }

    @Override
    TodoListTestData createTestDataGenerator() {
        return new TodoListTestData()
    }

    @Override
    void init(TodoListTestData testData) {
        this.endpoint = new TodoListEndpoint(this)
    }

    @Test
    void testUpdateDoneStatus()
    {
        def created = endpoint.create(testData.getCreateTestData())
        def updated = endpoint.updateDoneStatus(created.id, ["done" : true])
        def retrieved = endpoint.getById(created.id)
        assertEquals(updated, retrieved)
    }
}
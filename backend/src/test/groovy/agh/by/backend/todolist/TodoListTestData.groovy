package agh.by.backend.todolist

import agh.by.backend.common.TestData
import org.springframework.http.HttpStatus

class TodoListTestData extends TestData {

    @Override
    def getCreateTestData() {
        return [
                title: 'Shopping'
        ]
    }

    @Override
    def getUpdateTestData() {
        return [
                title: 'Sport shop'
        ]
    }

    @Override
    Object[][] getBadRequestData() {
        return [
                [HttpStatus.BAD_REQUEST, [title: null]],
                [HttpStatus.BAD_REQUEST, [title: 'a' * 51]]
        ]
    }
}

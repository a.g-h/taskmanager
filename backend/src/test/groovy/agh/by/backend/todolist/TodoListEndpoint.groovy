package agh.by.backend.todolist

import agh.by.backend.common.BaseEndpoint
import agh.by.backend.common.BaseTest
import org.springframework.http.HttpMethod
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.ResultMatcher

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class TodoListEndpoint extends BaseEndpoint{

    TodoListEndpoint(BaseTest baseTest) {
        super(baseTest, 'todo-list')
    }

    def updateDoneStatus(
            def id,
            def content,
            ResultMatcher expectedStatus = status().isOk())
    {
        def result = createRequest(
                HttpMethod.PUT, "/${endpointPath}/${id}/done",
                expectedStatus, content
        )

        return parseJson(result)
    }
}

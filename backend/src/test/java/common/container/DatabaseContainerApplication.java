package common.container;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.testcontainers.containers.MySQLContainer;

@Configuration
@EnableAutoConfiguration(exclude = {
        DataSourceAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class
})
@ComponentScan
public class DatabaseContainerApplication {
    @Autowired
    private ConfigurableApplicationContext context;

    private MySQLContainer mysql;

    private final Log logger = LogFactory.getLog(getClass());

    private static final String DATASOURCE_PORT = "datasource.port";

    @Value("${datasource.schema}")
    private String schema;

    public static DatabaseContainerApplication start(final String... args) {
        final ConfigurableApplicationContext ctx =
                new SpringApplicationBuilder(DatabaseContainerApplication.class)
                        .run(args);

        return ctx.getBean(DatabaseContainerApplication.class);
    }

    public void startDBContainer() {
        if (schema != null) {
            mysql = new MySQLContainer()
                    .withDatabaseName("taskmanager")
                    .withUsername("root")
                    .withPassword("");

            mysql.start();

            logger.info("Database started");

            System.setProperty(DATASOURCE_PORT, String.valueOf(mysql.getMappedPort(MySQLContainer.MYSQL_PORT)));

        }
    }

    public void stopDBContainer() {
        if (mysql != null) {
            logger.info("Stopping database");

            mysql.stop();

            logger.info("Database stopped");
        }

        SpringApplication.exit(context);
    }

}

package agh.by.backend.task;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TaskRepository extends JpaRepository<Task, UUID> {
    @Modifying
    @Query( value = "DELETE FROM Task t WHERE t.todoList.id = :listId" )
    void deleteAllByTodoListId( @Param( "listId" ) UUID listId );

    Optional<Task> findByTodoListIdAndId(UUID todoListId, UUID id );

    List<Task> findAllByTodoListId(UUID todoListId);
}

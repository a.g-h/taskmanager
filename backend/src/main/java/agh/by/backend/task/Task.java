package agh.by.backend.task;

import agh.by.backend.todolist.TodoList;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Data
@Entity
@Table( name = "task" )
public class Task {

    @EqualsAndHashCode.Exclude
    @Id
    @GeneratedValue
    @Column( name = "t_id" )
    private UUID id;

    /**
     * Task title
     */
    @NotBlank
    @Size( max = 50 )
    @Column( name = "t_title", nullable = false )
    private String title;

    /**
     * Task note
     */
    @Size( max = 500 )
    @Column( name = "t_note" )
    private String note;

    /**
     * Indicates whether task was done
     */
    @Column( name = "t_is_done" )
    @NotNull
    private Boolean isDone = false;

    /**
     * Parent list
     */
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "tl_id", nullable = false )
    private TodoList todoList;
}

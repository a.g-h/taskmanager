package agh.by.backend.task.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class TaskBaseVO {
    /**
     * Task title
     */
    @NotBlank
    @Size( min = 1, max = 50 )
    private String title;

    /**
     * Note to a task
     */
    @Size( min = 1, max = 500 )
    private String note;
}

package agh.by.backend.task;

import agh.by.backend.common.exception.NotFoundError;
import agh.by.backend.common.mapper.BaseMapper;
import agh.by.backend.common.service.BaseService;
import agh.by.backend.common.vo.UpdateStatusVO;
import agh.by.backend.task.vo.TaskRequestVO;
import agh.by.backend.task.vo.TaskResponseVO;
import agh.by.backend.todolist.TodoList;
import agh.by.backend.todolist.TodoListRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskService extends BaseService<Task, TaskRequestVO, TaskResponseVO> {

    private final TaskRepository repository;
    private final TodoListRepository listRepository;
    private final TaskVOMapper mapper;

    @Override
    protected BaseMapper<Task, TaskRequestVO, TaskResponseVO> getMapper() {
        return mapper;
    }

    @Override
    protected JpaRepository<Task, UUID> getRepository() {
        return repository;
    }

    @Override
    protected Task createEntity(TaskRequestVO vo, Object... args) {
        UUID listId = getTodoListId(args);

        TodoList list = listRepository.findById(listId).
                orElseThrow(() -> new NotFoundError(TodoList.class.getSimpleName(), listId));

        final Task task = mapper.toNewEntity(vo, list);

        return repository.save(task);
    }

    @Override
    protected Task updateEntity(Task task, TaskRequestVO vo, Object... args) {
        UUID listId = getTodoListId(args);

        TodoList list = listRepository.findById(listId).
                orElseThrow(() -> new NotFoundError(TodoList.class.getSimpleName(), listId));

        if (!task.getTodoList().getId().equals(listId)) {
            throw new NotFoundError(getEntityClass(), listId);
        }

        mapper.updateEntity(task, vo, list);
        return task;
    }

    @Override
    protected String getEntityClass() {
        return Task.class.getSimpleName();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public TaskResponseVO updateStatus(UUID id, UpdateStatusVO vo, UUID listId) {
        log.debug("Updating done flag for task = {}", id);

        final Task task = repository.findByTodoListIdAndId(listId, id)
                .orElseThrow(() -> new NotFoundError(getEntityClass(), id));

        task.setIsDone(vo.getDone());

        return mapper.toResponseVO(task);
    }

    @Override
    protected Task getEntity(UUID id, Object... args) {
        UUID listId = getTodoListId(args);
        return repository.findByTodoListIdAndId(listId, id)
                .orElseThrow(() -> new NotFoundError(getEntityClass(), id));
    }

    @Override
    public List<TaskResponseVO> getAll(Object... args) {
        UUID listId = getTodoListId(args);
        List<Task> tasks = repository.findAllByTodoListId(listId);

        return tasks.stream()
                .map(mapper::toResponseVO)
                .collect(Collectors.toList());
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteAllByListId(UUID listId) {
        repository.deleteAllByTodoListId(listId);
    }

    private UUID getTodoListId(Object... args) {
        if (!(args[0] instanceof UUID)) {
            throw new IllegalArgumentException();
        }

        return (UUID) args[0];
    }
}

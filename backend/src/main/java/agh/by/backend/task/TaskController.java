package agh.by.backend.task;


import agh.by.backend.common.vo.UpdateStatusVO;
import agh.by.backend.task.vo.TaskRequestVO;
import agh.by.backend.todolist.vo.TodoListRequestVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/todo-list/{listId}/task")
public class TaskController {

    private final TaskService service;

    /**
     * Create new task
     */
    @PostMapping
    public ResponseEntity createTask(@PathVariable("listId") UUID listId, @Valid @RequestBody TaskRequestVO vo) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.create(vo, listId));
    }


    /**
     * Get task by id
     */
    @GetMapping("/{id}")
    public ResponseEntity getTask(@PathVariable("listId") UUID listId, @PathVariable UUID id) {
        return ResponseEntity.ok().body(service.get(id, listId));
    }

    /**
     * Get all tasks
     *
     * @return list of tasks
     */
    @GetMapping
    public ResponseEntity getTasks(@PathVariable("listId") UUID listId) {
        return ResponseEntity.ok().body(service.getAll(listId));
    }

    /**
     * Delete task by id
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteTodoList(@PathVariable("listId") UUID listId, @PathVariable UUID id) {
        service.delete(id, listId);

        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Update task
     */
    @PutMapping("/{id}")
    public ResponseEntity updateTodoList(@PathVariable("listId") UUID listId, @PathVariable UUID id,
                                         @Valid @RequestBody TaskRequestVO vo) {
        return ResponseEntity.ok().body(service.update(vo, id, listId));
    }

    /**
     * Delete all tasks by to-do list id
     */
    @DeleteMapping
    public ResponseEntity deleteTasks(@PathVariable("listId") UUID listId) {
        service.deleteAllByListId(listId);

        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Update isDone flag
     */
    @PutMapping("/{id}/done")
    public ResponseEntity updateStatusOfItemCategory(@PathVariable("listId") UUID listId, @PathVariable final UUID id,
                                                     @Valid @RequestBody UpdateStatusVO vo) {
        return ResponseEntity.ok().body(service.updateStatus(id, vo, listId));
    }
}

package agh.by.backend.task;

import agh.by.backend.common.mapper.BaseMapper;
import agh.by.backend.task.vo.TaskRequestVO;
import agh.by.backend.task.vo.TaskResponseVO;
import agh.by.backend.todolist.TodoList;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TaskVOMapper implements BaseMapper<Task, TaskRequestVO, TaskResponseVO> {

    private final ModelMapper modelMapper = new ModelMapper();

    public TaskVOMapper() {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }

    @Override
    public TaskResponseVO toResponseVO(Task task, Object... args) {
        return modelMapper.map(task, TaskResponseVO.class);
    }

    @Override
    public Task toNewEntity(TaskRequestVO vo, Object... args) {
        if( args == null || args.length == 0 || !( args[0] instanceof TodoList) )
        {
            throw new IllegalArgumentException( "Required TodoList" );
        }

        final TodoList list = (TodoList) args[0];

        Task task = modelMapper.map(vo, Task.class);
        task.setTodoList(list);
        return task;
    }

    @Override
    public void updateEntity(Task task, TaskRequestVO vo, Object... args) {
        if( args == null || args.length == 0 || !( args[0] instanceof TodoList) )
        {
            throw new IllegalArgumentException( "Required TodoList" );
        }

        final TodoList list = (TodoList) args[0];

        modelMapper.map(vo, task);
        task.setTodoList(list);
    }
}

package agh.by.backend.common.vo;

import lombok.Data;

@Data
public class UpdateStatusVO {
    /**
     * Done flag
     */
    private Boolean done;
}

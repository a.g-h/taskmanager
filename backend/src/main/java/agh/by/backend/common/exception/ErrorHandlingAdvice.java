package agh.by.backend.common.exception;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.stream.Collectors;

@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class ErrorHandlingAdvice {

    @ExceptionHandler
    public ResponseEntity handle( final Throwable throwable )
    {
        log.error(throwable.getMessage());
        return new ResponseEntity(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Something went wrong!"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity handle(NotFoundError ex)
    {
        return new ResponseEntity(new ErrorResponse(HttpStatus.NOT_FOUND, ex.toString()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity handle(MethodArgumentNotValidException ex)
    {
        String errors = ex.getBindingResult().getFieldErrors().stream().map(FieldError::getField).collect(Collectors.joining());
        return new ResponseEntity(new ErrorResponse(HttpStatus.BAD_REQUEST, errors), HttpStatus.BAD_REQUEST);
    }
}

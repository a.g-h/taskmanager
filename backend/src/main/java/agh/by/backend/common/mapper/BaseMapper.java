package agh.by.backend.common.mapper;

public interface BaseMapper<ENTITY, REQUEST_VO, RESPONSE_VO> {

    RESPONSE_VO toResponseVO(ENTITY entity, Object... args);

    ENTITY toNewEntity(REQUEST_VO vo, Object... args);

    void updateEntity(ENTITY entity, REQUEST_VO vo, Object... args);
}

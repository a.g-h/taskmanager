package agh.by.backend.common.service;

import agh.by.backend.common.exception.NotFoundError;
import agh.by.backend.common.mapper.BaseMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @param <ENTITY>      Entity class
 * @param <REQUEST_VO>> Request value object class to create/update entity
 * @param <RESPONSE_VO> Response value object class to create/update entity
 */
@Slf4j
public abstract class BaseService<ENTITY, REQUEST_VO, RESPONSE_VO> {

    protected abstract BaseMapper<ENTITY, REQUEST_VO, RESPONSE_VO> getMapper();

    protected abstract JpaRepository<ENTITY, UUID> getRepository();

    protected abstract ENTITY createEntity(REQUEST_VO vo, Object... args);

    protected abstract ENTITY updateEntity(ENTITY entity, REQUEST_VO updateVO, Object... args);

    protected ENTITY getEntity(UUID id, Object... args) {
        return getRepository().findById(id)
                .orElseThrow( () -> new NotFoundError(getEntityClass(), id));
    }

    protected abstract String getEntityClass();

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public RESPONSE_VO get(UUID id, Object... args) {
        return getMapper().toResponseVO(getEntity(id, args));
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<RESPONSE_VO> getAll(Object... args) {
        return getRepository().findAll().stream()
                .map(getMapper()::toResponseVO)
                .collect(Collectors.toList());
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public RESPONSE_VO create(REQUEST_VO vo, Object... args) {
        return getMapper().toResponseVO(createReturnEntity(vo, args));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ENTITY createReturnEntity(REQUEST_VO vo, Object... args) {
        log.debug("Creating {}", getEntityClass());
        return createEntity(vo, args);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public RESPONSE_VO update(REQUEST_VO vo, UUID id, Object... args) {
        return getMapper().toResponseVO(updateReturnEntity(vo, id, args));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ENTITY updateReturnEntity(REQUEST_VO vo, UUID id, Object... args) {
        log.debug("Updating {} = {}", getEntityClass(), id);
        ENTITY entity = getEntity(id, args);
        return updateEntity(entity, vo, args);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Void delete(UUID id, Object... args) {
        log.debug("Removing {} = {}", getEntityClass(), id);

        ENTITY entity = getEntity(id, args);
        getRepository().delete(entity);

        return null;
    }
}

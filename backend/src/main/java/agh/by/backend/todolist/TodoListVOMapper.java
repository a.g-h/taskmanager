package agh.by.backend.todolist;

import agh.by.backend.common.mapper.BaseMapper;
import agh.by.backend.todolist.vo.TodoListRequestVO;
import agh.by.backend.todolist.vo.TodoListResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TodoListVOMapper implements BaseMapper<TodoList, TodoListRequestVO, TodoListResponseVO> {

    private final ModelMapper modelMapper = new ModelMapper();

    public TodoListVOMapper()
    {
        modelMapper.getConfiguration().setMatchingStrategy( MatchingStrategies.STRICT );
    }

    @Override
    public TodoListResponseVO toResponseVO(TodoList todoList, Object... args) {
        return modelMapper.map( todoList, TodoListResponseVO.class );
    }

    @Override
    public TodoList toNewEntity(TodoListRequestVO vo, Object... args) {
        return modelMapper.map( vo, TodoList.class );
    }

    @Override
    public void updateEntity(TodoList todoList, TodoListRequestVO vo, Object... args) {
        modelMapper.map( vo, todoList );
    }
}

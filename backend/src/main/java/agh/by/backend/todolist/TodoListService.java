package agh.by.backend.todolist;

import agh.by.backend.common.mapper.BaseMapper;
import agh.by.backend.common.service.BaseService;
import agh.by.backend.common.vo.UpdateStatusVO;
import agh.by.backend.todolist.vo.TodoListRequestVO;
import agh.by.backend.todolist.vo.TodoListResponseVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class TodoListService extends BaseService<TodoList, TodoListRequestVO, TodoListResponseVO> {

    private final TodoListRepository repository;
    private final TodoListVOMapper mapper;

    @Override
    protected BaseMapper<TodoList, TodoListRequestVO, TodoListResponseVO> getMapper() {
        return mapper;
    }

    @Override
    protected JpaRepository<TodoList, UUID> getRepository() {
        return repository;
    }

    @Override
    protected TodoList createEntity(TodoListRequestVO vo, Object... args) {
        final TodoList todoList = mapper.toNewEntity( vo );
        return repository.save(todoList);
    }

    @Override
    protected TodoList updateEntity(TodoList todoList, TodoListRequestVO updateVO, Object... args) {
        mapper.updateEntity(todoList, updateVO);
        return todoList;
    }

    @Override
    protected String getEntityClass() {
        return TodoList.class.getSimpleName();
    }

    @Transactional( propagation = Propagation.REQUIRED )
    public TodoListResponseVO updateStatus(UUID id, UpdateStatusVO vo) {
        log.debug("Updating done flag for todo list = {}", id);
        TodoList entity = getEntity(id);
        entity.setIsDone( vo.getDone() );

        return mapper.toResponseVO(entity);
    }
}

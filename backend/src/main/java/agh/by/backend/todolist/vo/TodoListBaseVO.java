package agh.by.backend.todolist.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class TodoListBaseVO {
    /**
     * Task title
     */
    @NotBlank
    @Size( min = 1, max = 50 )
    private String title;
}

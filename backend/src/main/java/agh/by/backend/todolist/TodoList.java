package agh.by.backend.todolist;

import agh.by.backend.task.Task;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table( name = "todo_list" )
public class TodoList {

    @EqualsAndHashCode.Exclude
    @Id
    @GeneratedValue
    @Column( name = "tl_id" )
    private UUID id;

    /**
     * To-do list title
     */
    @NotBlank
    @Size( max = 50 )
    @Column( name = "tl_title", nullable = false )
    private String title;

    /**
     * Indicates whether to-do list was done
     */
    @Column( name = "tl_is_done" )
    @NotNull
    private Boolean isDone = false;

    /**
     * List of tasks for this list
     */
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany( mappedBy = "todoList", cascade = CascadeType.REMOVE, orphanRemoval = true, fetch = FetchType.LAZY )
    private List<Task> tasks = new ArrayList<>();
}

package agh.by.backend.todolist.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TodoListResponseVO extends TodoListBaseVO {
    private UUID id;
    private Boolean isDone;
}

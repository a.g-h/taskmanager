package agh.by.backend.todolist;


import agh.by.backend.common.vo.UpdateStatusVO;
import agh.by.backend.todolist.vo.TodoListRequestVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/todo-list")
public class TodoListController {

    private final TodoListService service;

    /**
     * Create new to-do list
     */
    @PostMapping
    public ResponseEntity createTodoList(@Valid @RequestBody TodoListRequestVO vo) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.create(vo));
    }


    /**
     * Get to-do list by id
     */
    @GetMapping("/{id}")
    public ResponseEntity getTodoList(@PathVariable UUID id) {
        return ResponseEntity.ok().body(service.get(id));
    }

    /**
     * Get all to-do lists
     *
     * @return list of to-do lists
     */
    @GetMapping
    public ResponseEntity getTodoLists() {
        return ResponseEntity.ok().body(service.getAll());
    }

    /**
     * Delete to-do list by id
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteTodoList(@PathVariable UUID id) {
        service.delete(id);

        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Update to-do list
     */
    @PutMapping("/{id}")
    public ResponseEntity updateTodoList(@PathVariable UUID id,
                                  @Valid @RequestBody TodoListRequestVO vo) {
        return ResponseEntity.ok().body(service.update(vo, id));
    }

    /**
     * Update isDone flag
     */
    @PutMapping("/{id}/done")
    public ResponseEntity updateDoneStatus(@PathVariable final UUID id,
                                                     @Valid @RequestBody UpdateStatusVO vo) {
        return ResponseEntity.ok().body(service.updateStatus(id, vo));
    }
}

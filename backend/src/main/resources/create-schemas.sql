use taskmanager;

CREATE TABLE IF NOT EXISTS todo_list
(
    tl_id      VARCHAR(36),
    tl_title   VARCHAR(50) NOT NULL,
    tl_is_done BOOLEAN     NOT NULL DEFAULT FALSE,
    PRIMARY KEY (tl_id)
);

CREATE TABLE IF NOT EXISTS task
(
    t_id      VARCHAR(36),
    t_title   VARCHAR(50),
    t_note    VARCHAR(500),
    tl_id VARCHAR(36) NOT NULL,
    t_is_done BOOLEAN     NOT NULL DEFAULT FALSE,
    PRIMARY KEY (t_id),
    INDEX (tl_id),
    CONSTRAINT fk_todo_list FOREIGN KEY (tl_id)
        REFERENCES todo_list (tl_id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);